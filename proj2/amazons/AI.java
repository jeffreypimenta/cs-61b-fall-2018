package amazons;

import java.util.Iterator;

import static amazons.Piece.*;

/** A Player that automatically generates moves.
 *  @author J. S. Pimenta
 */
class AI extends Player {

    /** A position magnitude indicating a win (for white if positive, black
     *  if negative). */
    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;
    /** A magnitude greater than a normal value. */
    private static final int INFTY = Integer.MAX_VALUE;

    /** A new AI with no piece or controller (intended to produce
     *  a template). */
    AI() {
        this(null, null);
    }

    /** A new AI playing PIECE under control of CONTROLLER. */
    AI(Piece piece, Controller controller) {
        super(piece, controller);
    }

    @Override
    Player create(Piece piece, Controller controller) {
        return new AI(piece, controller);
    }

    @Override
    String myMove() {
        Move move = findMove();
        _controller.reportMove(move);
        return move.toString();
    }


    /** Return a move for me from the current position, assuming there
     *  is a move. */
    private Move findMove() {
        Board b = new Board(board());

        if (_myPiece == WHITE) {
            findMove(b, maxDepth(b), true, 1, -INFTY, INFTY);
        } else {
            findMove(b, maxDepth(b), true, -1, -INFTY, INFTY);
        }
        return _lastFoundMove;
    }

    /** The move found by the last call to one of the ...FindMove methods
     *  below. */
    private Move _lastFoundMove;

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _lastFoundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _lastMoveFound. */
    private int findMove(Board board, int depth, boolean saveMove, int sense,
                         int alpha, int beta) {
        if (depth == 0 || board.winner() != null) {
            return staticScore(board);
        }
        Move best = null;
        int bestScore;
        if (sense == 1) {
            bestScore = -INFTY;
            Iterator<Move> mAI = board.legalMoves();
            while (mAI.hasNext()) {
                Move m = mAI.next();
                Board next = new Board(board);
                next.makeMove(m);
                int resp = findMove(next, depth - 1, false, -1, alpha, beta);
                next.undo();
                if (best == null || resp >= bestScore) {
                    if (resp == bestScore) {
                        if (_controller.randInt(1) == 0) {
                            best = m;
                        }
                    } else {
                        best = m;
                    }
                    bestScore = resp;
                    alpha = Math.max(alpha, resp);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
        } else {
            bestScore = INFTY;
            Iterator<Move> mAI = board.legalMoves();
            while (mAI.hasNext()) {
                Move m = mAI.next();
                Board next = new Board(board);
                next.makeMove(m);
                int resp = findMove(next, depth - 1, false, 1, alpha, beta);
                next.undo();
                if (best == null || resp <= bestScore) {
                    if (resp == bestScore) {
                        if (_controller.randInt(1) == 0) {
                            best = m;
                        }
                    } else {
                        best = m;
                    }
                    bestScore = resp;
                    beta = Math.min(beta, resp);
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
        }
        if (saveMove) {
            _lastFoundMove = best;
        }
        return bestScore;
    }

    /** Number of possible branches for a given board.
     * @param board input board
     * @return number of branches*/
    private int branches(Board board) {
        Iterator<Move> it = board.legalMoves();
        int count = 0;
        while (it.hasNext()) {
            count++;
            it.next();
        }
        return count;
    }

    /** Return a heuristically determined maximum search depth
     *  based on characteristics of BOARD. */
    private int maxDepth(Board board) {
        int fanOut = branches(board);
        if (fanOut > boundOne) {
            return 1;
        }
        if (fanOut > boundTwo) {
            return 2;
        }
        if (fanOut > boundThree) {
            return 3;
        }
        if (fanOut > boundFour) {
            return 4;
        }
        return 5;
    }

    /** Returns true if spear lands on a neighboring square of a queen.
     * @param board input board. */
    private boolean spearProximity(Board board) {
        Piece color = board.turn();
        Square spear = board.lastMove().spear();
        Square neighbor;

        for (int i = 0; i < 8; i++) {
            neighbor = spear.queenMove(i, 1);
            if (neighbor != null && board.get(neighbor) == color) {
                return true;
            }
        }
        return false;
    }

    /** Count of moves.
     * @param board input board.
     * @return number of counts.*/
    private int moveCount(Board board) {
        Iterator<Square> sq = Square.iterator();
        int whiteCount = 0, blackCount = 0;
        while (sq.hasNext()) {
            Square st = sq.next();
            if (board.get(st) == WHITE) {
                Iterator<Square> rf = board.reachableFrom(st, null);
                while (rf.hasNext()) {
                    rf.next();
                    whiteCount++;
                }
            } else if (board.get(st) == BLACK) {
                Iterator<Square> rf = board.reachableFrom(st, null);
                while (rf.hasNext()) {
                    rf.next();
                    blackCount++;
                }
            }
        }
        return whiteCount - blackCount;
    }

    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        Piece winner = board.winner();
        if (winner != null) {
            if (winner == BLACK) {
                return -WINNING_VALUE;
            } else if (winner == WHITE) {
                return WINNING_VALUE;
            }
        }

        if (board.numMoves() < boundFour) {
            if (spearProximity(board)) {
                return moveCount(board) * 1000;
            }
        }
        return moveCount(board);
    }

    /** Bound #1. */
    private final int boundOne = 500;
    /** Bound #2. */
    private final int boundTwo = 100;
    /** Bound #3. */
    private final int boundThree = 40;
    /** Bound #4. */
    private final int boundFour = 20;

}
