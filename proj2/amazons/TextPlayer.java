package amazons;

/** A Player that takes input as text commands from the standard input.
 *  @author J. S. Pimenta
 */
class TextPlayer extends Player {

    /** A new TextPlayer with no piece or controller (intended to produce
     *  a template). */
    TextPlayer() {
        this(null, null);
    }

    /** A new TextPlayer playing PIECE under control of CONTROLLER. */
    private TextPlayer(Piece piece, Controller controller) {
        super(piece, controller);
    }

    @Override
    Player create(Piece piece, Controller controller) {
        return new TextPlayer(piece, controller);
    }

    @Override
    String myMove() {
        while (true) {
            String line = _controller.readLine();
            if (line == null) {
                return "quit";
            } else if (!line.contains("dump")
                    && !line.contains("new")
                    && !line.contains("seed")
                    && !line.contains("quit")
                    && !line.contains("undo")
                    && !line.contains("manual")
                    && !line.contains("auto")
                    && !Move.isGrammaticalMove(line)) {
                _controller.reportError("Invalid move. "
                                        + "Please try again.");
                continue;
            } else {
                if (Move.isGrammaticalMove(line)) {
                    if (!_controller.board().isLegal(Move.mv(line))) {
                        _controller.reportError("Not a legal move. "
                                + "Please try again.");
                        continue;
                    }
                    if (_controller.board().winner() != null) {
                        _controller.reportNote("%s:%s",
                                "Game over, ignoring move ", line);
                    }
                }
                return line;
            }
        }
    }
}
