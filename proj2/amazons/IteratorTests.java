package amazons;
import org.junit.Test;

import static amazons.Piece.BLACK;
import static amazons.Piece.WHITE;
import static org.junit.Assert.*;
import ucb.junit.textui;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/** Junit tests for our Board iterators.
 *  @author J. S. Pimenta
 */
public class IteratorTests {

    /** Run the JUnit tests in this package. */
    public static void main(String[] ignored) {
        textui.runClasses(IteratorTests.class);
    }

    /** Tests reachableFromIterator to make sure it returns all reachable
     *  Squares. This method may need to be changed based on
     *   your implementation. */
    @Test
    public void testReachableFrom() {
        Board b = new Board();
        buildBoard(b, REACHABLEFROMTESTBOARD);
        int numSquares = 0;
        Set<Square> squares = new HashSet<>();
        Iterator<Square> reachableFrom = b.reachableFrom(Square.sq(5, 5), null);
        while (reachableFrom.hasNext()) {
            Square s = reachableFrom.next();
            assertTrue(REACHABLEFROMTESTSQUARES.contains(s));
            numSquares += 1;
            squares.add(s);
        }
        assertEquals(REACHABLEFROMTESTSQUARES.size(), numSquares);
        assertEquals(REACHABLEFROMTESTSQUARES.size(), squares.size());
    }

    /** Tests legalMovesIterator to make sure it returns all legal Moves.
     *  This method needs to be finished and may need to be changed
     *  based on your implementation. */
    @Test
    public void testLegalMoves() {
        Board b = new Board();
        buildBoard(b, LEGALMOVESTESTBOARD);
        int numMoves = 0;
        Set<Move> moves = new HashSet<>();
        Iterator<Move> legalMoves = b.legalMoves(Piece.WHITE);
        while (legalMoves.hasNext()) {
            Move m = legalMoves.next();
            assertTrue(LEGALMOVESSET.contains(m));
            numMoves += 1;
            moves.add(m);
        }
        assertEquals(LEGALMOVESSET.size(), numMoves);
        assertEquals(LEGALMOVESSET.size(), moves.size());
    }

    @Test
    public void testWinning() {
        Board b = new Board();
        buildBoard(b, WINNINGTESTBOARD);
        b.makeMove(Square.sq("a4"), Square.sq("b4"), Square.sq("c4"));
        assertNotEquals(null, b.winner());
    }

    private void buildBoard(Board b, Piece[][] target) {
        for (int col = 0; col < Board.SIZE; col++) {
            for (int row = 0; row < Board.SIZE; row++) {
                Piece piece = target[row][col];
                b.put(piece, Square.sq(col, row));
            }
        }
    }

    @Test
    public void testStaticScore() {
        Board b = new Board();
        assertEquals(0, staticScore(b));

        buildBoard(b, WINNINGTESTBOARD);
        b.makeMove(Square.sq("a4"), Square.sq("b4"), Square.sq("c4"));
        assertEquals(WINNING_VALUE, staticScore(b));
    }

    private int numMoves(Board b) {
        Iterator<Square> sq = Square.iterator();
        int wCnt = 0, bCnt = 0;
        while (sq.hasNext()) {
            Square st = sq.next();
            if (b.get(st) == WHITE) {
                Iterator<Square> rf = b.reachableFrom(st, null);
                while (rf.hasNext()) {
                    rf.next();
                    wCnt++;
                }
            } else if (b.get(st) == BLACK) {
                Iterator<Square> rf = b.reachableFrom(st, null);
                while (rf.hasNext()) {
                    rf.next();
                    bCnt++;
                }
            }
        }
        return wCnt - bCnt;
    }

    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;

    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        Piece winner = board.winner();
        if (winner != null) {
            if (winner == BLACK) {
                return -WINNING_VALUE;
            } else if (winner == WHITE) {
                return WINNING_VALUE;
            }
        }
        return numMoves(board);
    }


    static final Piece E = Piece.EMPTY;

    static final Piece W = Piece.WHITE;

    static final Piece B = Piece.BLACK;

    static final Piece S = Piece.SPEAR;

    static final Piece[][] REACHABLEFROMTESTBOARD = {
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, W, W },
            { E, E, E, E, E, E, E, S, E, S },
            { E, E, E, S, S, S, S, E, E, S },
            { E, E, E, S, E, E, E, E, B, E },
            { E, E, E, S, E, W, E, E, B, E },
            { E, E, E, S, S, S, B, W, B, E },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
    };

    static final Piece[][] LEGALMOVESTESTBOARD = {
            { W, E, E, S, E, E, S, E, E, E },
            { E, E, S, S, E, E, E, E, E, E },
            { E, S, S, E, E, E, E, E, E, E },
            { S, S, E, E, E, E, E, E, E, S },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { B, E, E, E, E, E, E, E, E, B },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, B, E, E, B, E, E, E },
    };

    static final Piece[][] AIMOVESTESTBOARD = {
            { B, E, E, S, E, E, S, E, E, E },
            { B, E, S, S, E, E, E, E, E, E },
            { B, S, S, E, E, E, E, E, E, E },
            { S, S, E, E, E, E, E, E, E, S },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { W, E, E, E, E, E, E, E, E, W },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, W, E, E, W, E, E, E },
    };

    static final Piece[][] WINNINGTESTBOARD = {
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { W, E, E, E, E, E, E, E, E, W },
            { E, E, E, E, E, E, E, E, E, E },
            { W, E, E, E, S, E, E, E, E, E },
            { E, E, E, E, E, E, E, E, E, E },
            { S, S, S, W, S, E, E, E, E, E },
            { S, S, S, S, S, S, E, E, E, E },
            { B, B, S, B, B, S, E, E, E, E },
    };

    static final Piece[][] AITESTBOARD = {
            { W, E, S, S, E, W, S, E, E, E },
            { E, S, E, S, S, E, E, S, E, E },
            { S, B, E, S, E, S, S, E, E, E },
            { E, E, E, S, S, E, S, S, S, E },
            { E, S, S, S, S, S, S, B, S, S },
            { S, S, S, B, E, S, S, S, E, S },
            { S, S, S, S, S, S, S, S, S, S },
            { S, W, S, E, S, S, S, E, S, S },
            { S, S, S, E, E, S, S, S, S, B },
            { S, S, E, E, S, E, S, S, W, S },
    };



    static final Set<Square> REACHABLEFROMTESTSQUARES =
            new HashSet<>(Arrays.asList(
                    Square.sq(5, 4),
                    Square.sq(4, 4),
                    Square.sq(4, 5),
                    Square.sq(6, 5),
                    Square.sq(7, 5),
                    Square.sq(6, 4),
                    Square.sq(7, 3),
                    Square.sq(8, 2)));

    static final Set<Move> LEGALMOVESSET =
            new HashSet<>(Arrays.asList(
                    Move.mv("a1-a2(a3)"),
                    Move.mv("a1-a2(b2)"),
                    Move.mv("a1-a2(b1)"),
                    Move.mv("a1-a2(a1)"),
                    Move.mv("a1-a3(b2)"),
                    Move.mv("a1-a3(c1)"),
                    Move.mv("a1-a3(a2)"),
                    Move.mv("a1-a3(a1)"),
                    Move.mv("a1-b2(c1)"),
                    Move.mv("a1-b2(b1)"),
                    Move.mv("a1-b2(a1)"),
                    Move.mv("a1-b2(a2)"),
                    Move.mv("a1-b2(a3)"),
                    Move.mv("a1-b1(b2)"),
                    Move.mv("a1-b1(c1)"),
                    Move.mv("a1-b1(a1)"),
                    Move.mv("a1-b1(a2)"),
                    Move.mv("a1-c1(b1)"),
                    Move.mv("a1-c1(a1)"),
                    Move.mv("a1-c1(b2)"),
                    Move.mv("a1-c1(a3)")));
}
