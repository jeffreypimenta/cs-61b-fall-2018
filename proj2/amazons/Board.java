package amazons;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;

import static amazons.Piece.*;

/** The state of an Amazons Game.
 *  @author J. S. Pimenta
 */
class Board {

    /** The number of squares on a side of the board. */
    static final int SIZE = 10;

    /** Initializes a game board with SIZE squares on a side in the
     *  initial position. */
    Board() {
        init();
    }

    /** Initializes a copy of MODEL. */
    Board(Board model) {
        copy(model);
    }

    /** Copies MODEL into me. */
    void copy(Board model) {
        this._turn = model._turn;
        this._winner = model._winner;
        this._game = new Piece[Board.SIZE][Board.SIZE];
        this._moves = new ArrayList<>();

        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                this._game[i][j] = model._game[i][j];
            }
        }

        this._moves.addAll(model._moves);
    }

    /** Clears the board to the initial position. */
    void init() {
        _game = new Piece[Board.SIZE][Board.SIZE];
        _moves = new ArrayList<>();

        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                put(EMPTY, j, i);
            }
        }
        put(WHITE, Square.sq("a4"));
        put(WHITE, Square.sq("d1"));
        put(WHITE, Square.sq("g1"));
        put(WHITE, Square.sq("j4"));
        put(BLACK, Square.sq("a7"));
        put(BLACK, Square.sq("d10"));
        put(BLACK, Square.sq("g10"));
        put(BLACK, Square.sq("j7"));
        _turn = WHITE;
        _winner = EMPTY;
    }

    /** Return the Piece whose move it is (WHITE or BLACK). */
    Piece turn() {
        return _turn;
    }

    /** Return the number of moves (that have not been undone) for this
     *  board. */
    int numMoves() {
        return _moves.size();
    }

    /** Return the winner in the current position, or null if the game is
     *  not yet finished. */
    Piece winner() {
        if (!legalMoves(_turn).hasNext()) {
            _winner = _turn.opponent();
            return _winner;
        }
        return null;
    }

    /** Return the contents the square at S. */
    final Piece get(Square s) {
        return get(s.col(), s.row());
    }

    /** Return the contents of the square at (COL, ROW), where
     *  0 <= COL, ROW <= 9. */
    final Piece get(int col, int row) {
        return _game[row][col];
    }

    /** Return the contents of the square at COL ROW. */
    final Piece get(char col, char row) {
        return get(col - 'a', row - '1');
    }

    /** Set square S to P. */
    final void put(Piece p, Square s) {
        put(p, s.col(), s.row());
    }

    /** Set square (COL, ROW) to P. */
    final void put(Piece p, int col, int row) {
        _game[row][col] = p;
        _winner = EMPTY;
    }

    /** Set square COL ROW to P. */
    final void put(Piece p, char col, char row) {
        put(p, col - 'a', row - '1');
    }

    /** Return true iff FROM - TO is an unblocked queen move on the current
     *  board, ignoring the contents of ASEMPTY, if it is encountered.
     *  For this to be true, FROM-TO must be a queen move and the
     *  squares along it, other than FROM and ASEMPTY, must be
     *  empty. ASEMPTY may be null, in which case it has no effect. */
    boolean isUnblockedMove(Square from, Square to, Square asEmpty) {
        if (!from.isQueenMove(to)) {
            return false;
        }
        int dir = from.direction(to);
        Square pointer = from;
        while (pointer != to) {
            pointer = pointer.queenMove(dir, 1);
            if (pointer == null) {
                return false;
            }
            if (this.get(pointer) != EMPTY) {
                if (pointer != asEmpty) {
                    return false;
                }
            }
        }
        return true;
    }

    /** Return true iff FROM is a valid starting square for a move. */
    boolean isLegal(Square from) {
        if (get(from) == this._turn) {
            return true;
        }
        return false;
    }

    /** Return true iff FROM-TO is a valid first part of move, ignoring
     *  spear throwing. */
    boolean isLegal(Square from, Square to) {
        if (isLegal(from)) {
            if (isUnblockedMove(from, to, null)) {
                return true;
            }
        }
        return false;
    }

    /** Return true iff FROM-TO(SPEAR) is a legal move in the current
     *  position. */
    boolean isLegal(Square from, Square to, Square spear) {
        if (isLegal(from, to)) {
            if (isUnblockedMove(to, spear, from)) {
                return true;
            }
        }
        return false;
    }

    /** Return true iff MOVE is a legal move in the current
     *  position. */
    boolean isLegal(Move move) {
        if (move == null) {
            return false;
        }
        return isLegal(move.from(), move.to(), move.spear());
    }

    /** Move FROM-TO(SPEAR), assuming this is a legal move. */
    void makeMove(Square from, Square to, Square spear) {
        this.put(EMPTY, from);
        this.put(_turn, to);
        this.put(SPEAR, spear);
        _moves.add(Move.mv(from, to, spear));
        _turn = _turn.opponent();
        _winner = winner();
    }

    /** Move according to MOVE, assuming it is a legal move. */
    void makeMove(Move move) {
        makeMove(move.from(), move.to(), move.spear());
    }

    /** Undo one move.  Has no effect on the initial board. */
    void undo() {
        Move undo = _moves.remove(_moves.size() - 1);
        Piece p = this.get(undo.to());
        this.put(EMPTY, undo.spear());
        this.put(EMPTY, undo.to());
        this.put(p, undo.from());
        _turn = _turn.opponent();
    }

    /** Return an Iterator over the Squares that are reachable by an
     *  unblocked queen move from FROM. Does not pay attention to what
     *  piece (if any) is on FROM, nor to whether the game is finished.
     *  Treats square ASEMPTY (if non-null) as if it were EMPTY.  (This
     *  feature is useful when looking for Moves, because after moving a
     *  piece, one wants to treat the Square it came from as empty for
     *  purposes of spear throwing.) */
    Iterator<Square> reachableFrom(Square from, Square asEmpty) {
        return new ReachableFromIterator(from, asEmpty);
    }

    /** Return an Iterator over all legal moves on the current board. */
    Iterator<Move> legalMoves() {
        return new LegalMoveIterator(_turn);
    }

    /** Return an Iterator over all legal moves on the current board for
     *  SIDE (regardless of whose turn it is). */
    Iterator<Move> legalMoves(Piece side) {
        return new LegalMoveIterator(side);
    }

    /** An iterator used by reachableFrom. */
    private class ReachableFromIterator implements Iterator<Square> {

        /** Iterator of all squares reachable by queen move from FROM,
         *  treating ASEMPTY as empty. */
        ReachableFromIterator(Square from, Square asEmpty) {
            _from = from;
            _dir = -1;
            _steps = 0;
            _asEmpty = asEmpty;
            toNext();
        }

        @Override
        public boolean hasNext() {
            return _dir < 8;
        }

        @Override
        public Square next() {
            Square to = _from.queenMove(_dir, _steps);
            toNext();
            return to;
        }

        /** Advance _dir and _steps, so that the next valid Square is
         *  _steps steps in direction _dir from _from. */
        private void toNext() {
            if (_steps == 0) {
                _dir += 1;
            }
            _steps += 1;
            while (true) {
                Square to = _from.queenMove(_dir, _steps);
                if (to == null) {
                    _steps = 1;
                    _dir += 1;
                    if (_dir > 7) {
                        break;
                    } else {
                        continue;
                    }
                }
                if (isUnblockedMove(_from, to, _asEmpty)) {
                    return;
                } else {
                    _steps = 1;
                    _dir += 1;
                    if (_dir > 7) {
                        break;
                    }
                }
            }
        }

        /** Starting square. */
        private Square _from;
        /** Current direction. */
        private int _dir;
        /** Current distance. */
        private int _steps;
        /** Square treated as empty. */
        private Square _asEmpty;
    }

    /** An iterator used by legalMoves. */
    private class LegalMoveIterator implements Iterator<Move> {

        /** All legal moves for SIDE (WHITE or BLACK). */
        LegalMoveIterator(Piece side) {
            _startingSquares = Square.iterator();
            _spearThrows = NO_SQUARES;
            _pieceMoves = NO_SQUARES;
            _fromPiece = side;
            toNext();
        }

        @Override
        public boolean hasNext() {
            return _spearThrows.hasNext()
                    || _pieceMoves.hasNext()
                    || _startingSquares.hasNext();
        }

        @Override
        public Move next() {
            Square spear = _spearThrows.next();
            Move mv = Move.mv(_start, _nextSquare, spear);
            if (mv != null && isLegal(_start, _nextSquare, spear)) {
                toNext();
                return mv;
            }
            return null;
        }

        /** Advance so that the next valid Move is
         *  _start-_nextSquare(sp), where sp is the next value of
         *  _spearThrows. */
        private void toNext() {
            if (_pieceMoves == NO_SQUARES) {
                _start = getStart();
            }
            if (_start == null) {
                return;
            }
            if (_spearThrows == NO_SQUARES || !_spearThrows.hasNext()) {
                _nextSquare = getNextSquare();
                if (_nextSquare == null) {
                    return;
                } else {
                    _spearThrows = reachableFrom(_nextSquare, _start);
                }
            }

        }

        /** Get starting squares.
         * @return Squares with queens. */
        private Square getStart() {
            while (_startingSquares.hasNext()) {
                Square st = _startingSquares.next();
                if (get(st) == _fromPiece) {
                    return st;
                }
            }
            return null;
        }

        /** Get the next square for to position.
         * @return Square that is next. */
        private Square getNextSquare() {
            if (_pieceMoves == NO_SQUARES) {
                _pieceMoves = reachableFrom(_start, null);
            }
            if (_pieceMoves.hasNext()) {
                return _pieceMoves.next();
            } else {
                _start = getStart();
                if (_start == null) {
                    return null;
                }
                _pieceMoves = reachableFrom(_start, null);
                return getNextSquare();
            }
        }

        /** Color of side whose moves we are iterating. */
        private Piece _fromPiece;
        /** Current starting square. */
        private Square _start;
        /** Remaining starting squares to consider. */
        private Iterator<Square> _startingSquares;
        /** Current piece's new position. */
        private Square _nextSquare;
        /** Remaining moves from _start to consider. */
        private Iterator<Square> _pieceMoves;
        /** Remaining spear throws from _piece to consider. */
        private Iterator<Square> _spearThrows;
    }

    @Override
    public String toString() {
        StringBuilder rstr = new StringBuilder();
        for (int i = Board.SIZE - 1; i >= 0; i--) {
            rstr.append("  ");
            for (int j = 0; j < Board.SIZE; j++) {
                rstr.append(" " + get(j, i));
            }
            rstr.append("\n");
        }
        return rstr.toString();
    }

    /** Returns the last move. */
    Move lastMove() {
        return _moves.get(numMoves() - 1);
    }

    /** An empty iterator for initialization. */
    private static final Iterator<Square> NO_SQUARES =
        Collections.emptyIterator();

    /** Piece whose turn it is (BLACK or WHITE). */
    private Piece _turn;
    /** Cached value of winner on this board, or EMPTY if it has not been
     *  computed. */
    private Piece _winner;
    /** Map of Piece and Square values. */
    private Piece[][] _game;
    /** Tracking the list of moves made so far. */
    private ArrayList<Move> _moves;

}
