package amazons;

import org.junit.Test;

import static amazons.Piece.*;
import static org.junit.Assert.*;
import ucb.junit.textui;

import java.util.Iterator;

/** The suite of all JUnit tests for the amazons package.
 *  @author J. S. Pimenta
 */
public class UnitTest {

    /** Run the JUnit tests in this package. Add xxxTest.class entries to
     *  the arguments of runClasses to run other JUnit tests. */
    public static void main(String[] ignored) {
        textui.runClasses(UnitTest.class);
        textui.runClasses(IteratorTests.class);
    }

    /** Tests basic correctness of put and get on the initialized board. */
    @Test
    public void testBasicPutGet() {
        Board b = new Board();
        b.put(BLACK, Square.sq(3, 5));
        assertEquals(b.get(3, 5), BLACK);
        b.put(WHITE, Square.sq(9, 9));
        assertEquals(b.get(9, 9), WHITE);
        b.put(EMPTY, Square.sq(3, 5));
        assertEquals(b.get(3, 5), EMPTY);
    }

    /** Tests proper identification of legal/illegal queen moves. */
    @Test
    public void testIsQueenMove() {
        assertFalse(Square.sq(1, 5).isQueenMove(Square.sq(1, 5)));
        assertFalse(Square.sq(1, 5).isQueenMove(Square.sq(2, 7)));
        assertFalse(Square.sq(0, 0).isQueenMove(Square.sq(5, 1)));
        assertTrue(Square.sq(1, 1).isQueenMove(Square.sq(9, 9)));
        assertTrue(Square.sq(2, 7).isQueenMove(Square.sq(8, 7)));
        assertTrue(Square.sq(3, 0).isQueenMove(Square.sq(3, 4)));
        assertTrue(Square.sq(7, 9).isQueenMove(Square.sq(0, 2)));
    }

    /** Tests toString for initial board state and a smiling board state. :) */
    @Test
    public void testToString() {
        Board b = new Board();
        assertEquals(INIT_BOARD_STATE, b.toString());
        makeSmile(b);
        assertEquals(SMILE, b.toString());
    }

    /** test legal_moves */
    @Test
    public void testLegalMoves() {
        Board b = new Board();
        assertFalse("illegal move", b.isLegal(Move.mv("g1-g7(j7)")));
        assertTrue(b.isLegal(Move.mv("g1-g7(i7)")));
        assertTrue(b.isLegal(Move.mv("g1-g7(g1)")));
        assertFalse("illegal move", b.isLegal(Move.mv("g1-g7(g10)")));
        assertFalse("illegal move", b.isLegal(Move.mv("g1-g7(h9)")));
        assertFalse("illegal move", b.isLegal(Move.mv("g1-d1(a1)")));
        assertTrue(b.isLegal(Move.mv("d1-f1(e1)")));
        assertTrue(b.isLegal(Move.mv("d1-e1(d1)")));
        assertFalse("illegal move", b.isLegal(Move.mv("a7-b7(c7)")));
        assertFalse("illegal move", b.isLegal(Move.mv("a1-b1(c1)")));
    }

    /** test reachableFrom iterator */
    @Test
    public void testReachableFrom() {
        Board lb = new Board();
        lb.put(SPEAR, Square.sq("b3"));
        lb.put(SPEAR, Square.sq("c2"));
        lb.put(SPEAR, Square.sq("c3"));
        lb.put(WHITE, Square.sq("a1"));
        Iterator<Square> lbR = lb.reachableFrom(Square.sq("a1"), null);
        StringBuilder actual = new StringBuilder();
        while (lbR.hasNext()) {
            actual.append(lbR.next());
        }
        assertEquals(actual.toString(), "a2a3b2b1c1");

        Board b = new Board();
        int cnt = 0;
        Iterator<Square> it = b.reachableFrom(Square.sq("a4"), null);
        while (it.hasNext()) {
            cnt++;
            it.next();
        }
        assertEquals(20, cnt);
    }

    /** test legalmoves iterator */
    @Test
    public void testLegalMovesIterator() {
        Board lb = new Board();
        Iterator<Move> it = lb.legalMoves(WHITE);
        int cnt = 0;
        while (it.hasNext()) {
            cnt++;
            it.next();
        }
        assertEquals(2176, cnt);
    }

    /** test spear proximity */
    @Test
    public void testSpearProximity() {
        Board lb = new Board();
        lb.makeMove(Square.sq("g1"), Square.sq("h1"), Square.sq("i1"));
        assertFalse("not close", spearProximity(lb));
        lb.makeMove(Square.sq("a7"), Square.sq("a8"), Square.sq("a9"));
        lb.makeMove(Square.sq("g1"), Square.sq("g2"), Square.sq("g9"));
        assertTrue(spearProximity(lb));
    }

    private boolean spearProximity(Board b) {
        Piece color = b.turn();
        Square spear = b.lastMove().spear();
        Square nbor = null;

        for (int i = 0; i < 8; i++) {
            nbor = spear.queenMove(i, 1);
            if (nbor != null && b.get(nbor) == color) {
                return true;
            }
        }
        return false;
    }

    private void makeSmile(Board b) {
        b.put(EMPTY, Square.sq(0, 3));
        b.put(EMPTY, Square.sq(0, 6));
        b.put(EMPTY, Square.sq(9, 3));
        b.put(EMPTY, Square.sq(9, 6));
        b.put(EMPTY, Square.sq(3, 0));
        b.put(EMPTY, Square.sq(3, 9));
        b.put(EMPTY, Square.sq(6, 0));
        b.put(EMPTY, Square.sq(6, 9));
        for (int col = 1; col < 4; col += 1) {
            for (int row = 6; row < 9; row += 1) {
                b.put(SPEAR, Square.sq(col, row));
            }
        }
        b.put(EMPTY, Square.sq(2, 7));
        for (int col = 6; col < 9; col += 1) {
            for (int row = 6; row < 9; row += 1) {
                b.put(SPEAR, Square.sq(col, row));
            }
        }
        b.put(EMPTY, Square.sq(7, 7));
        for (int lip = 3; lip < 7; lip += 1) {
            b.put(WHITE, Square.sq(lip, 2));
        }
        b.put(WHITE, Square.sq(2, 3));
        b.put(WHITE, Square.sq(7, 3));
    }

    static final String INIT_BOARD_STATE =
            "   - - - B - - B - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   B - - - - - - - - B\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   W - - - - - - - - W\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - W - - W - - -\n";

    static final String SMILE =
            "   - - - - - - - - - -\n"
                    + "   - S S S - - S S S -\n"
                    + "   - S - S - - S - S -\n"
                    + "   - S S S - - S S S -\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - W - - - - W - -\n"
                    + "   - - - W W W W - - -\n"
                    + "   - - - - - - - - - -\n"
                    + "   - - - - - - - - - -\n";

}
