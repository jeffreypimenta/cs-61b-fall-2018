package enigma;

/** Class that represents a rotating rotor in the enigma machine.
 *  @author J. S. Pimenta
 */
class MovingRotor extends Rotor {

    /** A rotor named NAME whose permutation in its default setting is
     *  PERM, and whose notches are at the positions indicated in NOTCHES.
     *  The Rotor is initally in its 0 setting (first character of its
     *  alphabet).
     */
    MovingRotor(String name, Permutation perm, String notches) {
        super(name, perm);
        _notches = new int[notches.length()];
        for (int i = 0; i < notches.length(); i++) {
            _notches[i] = permutation().alphabet().toInt(notches.charAt(i));
        }
    }

    @Override
    boolean atNotch() {
        for (int notch : _notches) {
            if (setting() == notch) {
                return true;
            }
        }
        return false;
    }

    @Override
    boolean rotates() {
        return true;
    }

    @Override
    void advance() {
        _setting = wrap(_setting + 1);
        this.movePerm(this.setting());
    }

    /** An intlist of the notches given in the constructor. */
    private int[] _notches;

}
