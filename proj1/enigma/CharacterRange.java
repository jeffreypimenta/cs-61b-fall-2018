package enigma;

import static enigma.EnigmaException.*;
import java.util.ArrayList;

/** An Alphabet consisting of the Unicode characters in a certain range in
 *  order.
 *  @author P. N. Hilfinger
 */
class CharacterRange extends Alphabet {

    /** Constructor that takes in an input string.
     * @param inputStr is the input string. */
    CharacterRange(String inputStr) {
        String str = inputStr.trim().toUpperCase();
        _alphArr = new ArrayList<>();
        if (str.charAt(1) == '-') {
            char c = str.charAt(0);
            while (c != str.charAt(2)) {
                _alphArr.add(c);
                c = (char) ((int) c + 1);
            }
            _alphArr.add(c);
        } else {
            for (int i = 0; i < str.length(); i++) {
                _alphArr.add(str.charAt(i));
            }
        }
    }

    @Override
    int size() {
        return _alphArr.size();
    }

    @Override
    boolean contains(char ch) {
        return _alphArr.contains(ch);
    }

    @Override
    char toChar(int index) {
        if (!contains((char) (_alphArr.get(index)))) {
            throw error("character index out of range");
        }
        return (char) (_alphArr.get(index));
    }

    @Override
    int toInt(char ch) {
        if (!contains(ch)) {
            throw error("character out of range");
        }
        return _alphArr.indexOf(ch);
    }

    /** Array list of input alphabet. */
    private ArrayList<Character> _alphArr;

}
