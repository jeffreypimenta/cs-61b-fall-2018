package enigma;

import java.util.ArrayList;

/** Represents a permutation of a range of integers starting at 0 corresponding
 *  to the characters of an alphabet.
 *  @author J. S. Pimenta
 */
class Permutation {

    /** Set this Permutation to that specified by CYCLES, a string in the
     *  form "(cccc) (cc) ..." where the c's are characters in ALPHABET, which
     *  is interpreted as a permutation in cycle notation.  Characters in the
     *  alphabet that are not included in any cycle map to themselves.
     *  Whitespace is ignored. */
    Permutation(String cycles, Alphabet alphabet) {
        _alphabet = alphabet;
        String[] cyclesArray = stripSplit(cycles);
        alphabetize(_alphabet);
        for (String c : cyclesArray) {
            addCycle(c);
        }
    }

    /** Secondary constructor that takes in a list to replace _assigned.
     * @param replacement is a list of inputs to outputs.
     * @param alphabet is the input alphabet.*/
    Permutation(ArrayList<Integer> replacement, Alphabet alphabet) {
        _alphabet = alphabet;
        _assigned = replacement;
    }

    /** Add the cycle c0->c1->...->cm->c0 to the permutation, where CYCLE is
     *  c0c1...cm. */
    private void addCycle(String cycle) {
        int input, output;
        for (int i = 0; i < cycle.length(); i++) {
            input = _alphabet.toInt(cycle.charAt(i));
            if (i == (cycle.length() - 1)) {
                output = _alphabet.toInt(cycle.charAt(0));
            } else {
                output = _alphabet.toInt(cycle.charAt(i + 1));
            }
            _assigned.set(input, output);
        }
    }

    /** Return the value of P modulo the size of this permutation. */
    final int wrap(int p) {
        int r = p % size();
        if (r < 0) {
            r += size();
        }
        return r;
    }

    /** Returns the size of the alphabet I permute. */
    int size() {
        return alphabet().size();
    }

    /** Return the result of applying this permutation to P modulo the
     *  alphabet size. */
    int permute(int p) {
        int wrapped = wrap(p);
        return _assigned.get(wrapped);
    }

    /** Return the result of applying the inverse of this permutation
     *  to C modulo the alphabet size. */
    int invert(int c) {
        int wrapped = wrap(c);
        return _assigned.indexOf(wrapped);
    }

    /** Return the result of applying this permutation to the index of P
     *  in ALPHABET, and converting the result to a character of ALPHABET. */
    char permute(char p) {
        int converted = _alphabet.toInt(p);
        return _alphabet.toChar(permute(converted));
    }

    /** Return the result of applying the inverse of this permutation to C. */
    int invert(char c) {
        int converted = _alphabet.toInt(c);
        return _alphabet.toChar(invert(converted));
    }

    /** Return the alphabet used to initialize this Permutation. */
    Alphabet alphabet() {
        return _alphabet;
    }

    /** Return true iff this permutation is a derangement (i.e., a
     *  permutation for which no value maps to itself). */
    boolean derangement() {
        for (int i = 0; i < _assigned.size(); i++) {
            int val = _assigned.get(i);
            if (val == _assigned.indexOf(val)) {
                return false;
            }
        }
        return true;
    }

    /** Initializes the ArrayList to the alphabet.
     * @param a The input alphabet. */
    private void alphabetize(Alphabet a) {
        _assigned = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            _assigned.add(i);
        }
    }

    /** Strips the parentheses from the input string s
     * and returns a string array of the substrings separated
     * by spaces.
     * @param s The string of cycles that will be split. */
    private String[] stripSplit(String s) {
        s = s.replaceAll("[)(]", " ");
        return s.split(" ");
    }

    /** Alphabet of this permutation. */
    private Alphabet _alphabet;

    /** An array list that maps inputs to outputs. */
    private ArrayList<Integer> _assigned;
}
