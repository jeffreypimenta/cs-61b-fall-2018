package enigma;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static enigma.EnigmaException.error;

public class MachineTest {
    Alphabet _alphabet;
    Machine enig;
    Scanner sc;
    Scanner _config;
    ArrayList<Rotor> allRotors;
    String setting1 = "* B BETA III IV I AXLE (YF) (ZH)";
    String setting2 = "* B BETA I II III AAAA";
    String configStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
            + " 5 3\n"
            + " I MQ      (AELTPHQXRU) (BKNW) (CMOY) (DFG) (IV) (JZ) (S)\n"
            + " II ME     (FIXVYOMW) (CDKLHUP) (ESZ) (BJ) (GR) (NT) "
            +             "(A) (Q)\n"
            + " III MV    (ABDHPEJT) (CFLVMZOYQIRWUKXSG) (N)\n"
            + " IV MJ     (AEPLIYWCOXMRFZBSTGJQNH) (DV) (KU)\n"
            + " V MZ      (AVOLDRWFIUQ)(BZKSMNHYC) (EGTJPX)\n"
            + " VI MZM    (AJQDVLEOZWIYTS) (CGMNHFUX) (BPRK) \n"
            + " VII MZM   (ANOUPFRIMBZTLWKSVEGCJYDHXQ) \n"
            + " VIII MZM  (AFLSETWUNDHOZVICQ) (BKJ) (GXY) (MPR)\n"
            + " Beta N    (ALBEVFCYODJWUGNMQTZSKPR) (HIX)\n"
            + " Gamma N   (AFNIRLBSQWVXGUZDKMTPCOYJHE)\n"
            + " B R       (AE) (BN) (CK) (DQ) (FU) (GY) (HW) (IJ) "
            +             "(LO) (MP)\n"
            + "           (RX) (SZ) (TV)\n"
            + " C R       (AR) (BD) (CO) (EJ) (FN) (GT) (HK) (IV) "
            +             "(LM) (PW)\n"
            + "           (QZ) (SX) (UY)";

    @Test
    public void testConvertMessage() {
        _config = new Scanner(configStr);
        enig = readConfig();
        setUp(enig, setting2.trim());

        String inmsg = "Hello world";
        assertEquals("ILBDAAMTAZ", enig.convert(inmsg));
    }

    /** Return an Enigma machine configured from the contents of configuration
     *  file _config. */
    private Machine readConfig() {
        try {
            if (_config.hasNextLine()) {
                String line1 = _config.nextLine().trim();
                _alphabet = new CharacterRange(line1);
            }

            int numRotors = 0;
            int numPawls = 0;

            if (_config.hasNextLine()) {
                String[] line2 = _config.nextLine().trim().split(" ");
                numRotors = Integer.parseInt(line2[0]);
                numPawls = Integer.parseInt(line2[1]);
            }

            int configSize = 50;
            String[] str = new String[configSize];
            int lineNum = 0;

            while (_config.hasNextLine()) {
                str[lineNum] = _config.nextLine().trim();
                if (str[lineNum].charAt(0) == '(') {
                    StringBuilder temp = new StringBuilder(str[lineNum - 1]);
                    temp.append(" " + str[lineNum]);
                    str[lineNum - 1] = temp.toString();
                    continue;
                }
                lineNum++;
            }

            StringBuilder stringer = new StringBuilder();
            for (int i = 0; i < lineNum; i++) {
                stringer.append(str[i]);
                stringer.append("\n");
            }

            _config = new Scanner(stringer.toString());
            ArrayList<Rotor> allR = new ArrayList<>();
            while (_config.hasNextLine()) {
                allR.add(readRotor());
            }

            return new Machine(_alphabet, numRotors, numPawls, allR);
        } catch (NoSuchElementException excp) {
            throw error("configuration file truncated");
        }
    }

    /** Return a rotor, reading its description from _config. */
    private Rotor readRotor() {
        try {
            Rotor r;
            String name, notches;
            char type;
            String str = _config.nextLine().toUpperCase();
            String str1 = str.substring(0, str.indexOf('('));
            String cycles = str.substring(str.indexOf('('));
            String[] rotorStr;
            rotorStr = str1.trim().split(" ");
            name = rotorStr[0];
            type = rotorStr[1].charAt(0);
            notches = rotorStr[1].substring(1);
            Permutation added = new Permutation(cycles, _alphabet);
            switch (type) {
            case 'M': r = new MovingRotor(name, added, notches);
                    break;
            case 'N': r = new FixedRotor(name, added);
                    break;
            case 'R': r = new Reflector(name, added);
                    break;
            default: throw error("Invalid rotor type %c", type);
            }
            return r;
        } catch (NoSuchElementException excp) {
            throw error("bad rotor description");
        }
    }

    /** Set M according to the specification given on SETTINGS,
     *  which must have the format specified in the assignment. */
    private void setUp(Machine M, String settings) {
        String setStr = settings.substring(1).trim().toUpperCase();
        int pIndex = setStr.indexOf('(');
        String rotSet, plugboard;
        if (pIndex == -1) {
            plugboard = "";
            rotSet = setStr;
        } else {
            rotSet = setStr.substring(0, pIndex);
            plugboard = setStr.substring(pIndex);
        }
        String[] rotorOrder = rotSet.split(" ");
        String setting = rotorOrder[rotorOrder.length - 1];
        String[] rotors = new String[rotorOrder.length - 1];
        System.arraycopy(rotorOrder, 0, rotors, 0, rotors.length);
        M.insertRotors(rotors);
        M.setRotors(setting);
        M.setPlugboard(new Permutation(plugboard, _alphabet));
    }


}


