package enigma;

import java.util.ArrayList;
import java.util.HashSet;

import static enigma.EnigmaException.*;

/** Class that represents a complete enigma machine.
 *  @author J. S. Pimenta
 */
class Machine {

    /** A new Enigma machine with alphabet ALPHA, 1 < NUMROTORS rotor slots,
     *  and 0 <= PAWLS < NUMROTORS pawls.  ALLROTORS contains all the
     *  available rotors. */
    Machine(Alphabet alpha, int numRotors, int pawls,
            ArrayList<Rotor> allRotors) {
        if (numRotors <= 1 || numRotors <= pawls || pawls < 0) {
            throw error("Incorrect number of rotors "
                    + "and/or pawls in config file");
        }
        _alphabet = alpha;
        _numRotors = numRotors;
        _pawls = pawls;
        _allRotors = allRotors;
    }

    /** Return the number of rotor slots I have. */
    int numRotors() {
        return _numRotors;
    }

    /** Return the number pawls (and thus rotating rotors) I have. */
    int numPawls() {
        return _pawls;
    }

    /** Set my rotor slots to the rotors named ROTORS from my set of
     *  available rotors (ROTORS[0] names the reflector).
     *  Initially, all rotors are set at their 0 setting. */
    void insertRotors(String[] rotors) {
        _activeRotors = new ArrayList<>();
        boolean found = false;
        for (String name : rotors) {
            for (Rotor r : _allRotors) {
                if (name.equals(r.name())) {
                    if (!_activeRotors.contains(r)) {
                        _activeRotors.add(r);
                        found = true;
                        break;
                    } else {
                        throw error("Rotor %s has been "
                                + "repeated in the setting", name);
                    }
                }
            }
            if (!found) {
                throw error("Rotor %s not found in "
                        + "config file rotors", name);
            }
            found = false;
        }
        if (!_activeRotors.get(0).reflecting()) {
            throw error("Rotor %s is not "
                    + "a reflector", _activeRotors.get(0).name());
        }
        int movingCount = 0;
        for (int i = 1; i < _activeRotors.size(); i++) {
            Rotor r = _activeRotors.get(i);
            if (r.reflecting()) {
                throw error("Cannot have more than one reflector");
            }
            if (!r.rotates() && _activeRotors.get(i - 1).rotates()) {
                throw error("Moving rotors not to right of fixed");
            }
            if (r.rotates()) {
                movingCount++;
            }
        }
        if (numPawls() != movingCount) {
            throw error("Not enough moving rotors");
        }
    }

    /** Set my rotors according to SETTING, which must be a string of
     *  numRotors()-1 upper-case letters. The first letter refers to the
     *  leftmost rotor setting (not counting the reflector).  */
    void setRotors(String setting) {
        if (!setting.matches("[a-zA-Z0-9]+")) {
            throw error("Invalid character in setting");
        }
        if (setting.length() != numRotors() - 1) {
            throw error("Initial rotor settings "
                    + "not completely specified");
        }
        int i = 0;
        for (Rotor r : _activeRotors) {
            if (r.reflecting()) {
                continue;
            }
            r.set(setting.charAt(i));
            i++;
        }
    }

    /** Set the plugboard to PLUGBOARD. */
    void setPlugboard(Permutation plugboard) {
        _plugboard = plugboard;
    }

    /** Returns false if there are no active rotors in the machine. */
    boolean isSetUp() {
        return _activeRotors != null;
    }

    /** Returns the result of converting the input character C (as an
     *  index in the range 0..alphabet size - 1), after first advancing
     *  the machine. */
    int convert(int c) {
        int p = _plugboard.permute(c);
        HashSet<Integer> adv = new HashSet<>();

        for (int i = numRotors() - 1, j = 0; j < numPawls() - 1; i--, j++) {
            if (_activeRotors.get(i).atNotch()) {
                if (i == numRotors() - 1) {
                    adv.add(i - 1);
                } else {
                    adv.add(i - 1);
                    adv.add(i);
                }
            }
        }

        adv.add(numRotors() - 1);

        for (Integer rnum : adv) {
            _activeRotors.get(rnum).advance();
        }

        for (int i = numRotors() - 1; i >= 0; i--) {
            p = _activeRotors.get(i).convertForward(p);
        }
        for (int i = 1; i < numRotors(); i++) {
            p = _activeRotors.get(i).convertBackward(p);
        }
        return _plugboard.invert(p);
    }

    /** Returns the encoding/decoding of MSG, updating the state of
     *  the rotors accordingly. */
    String convert(String msg) {
        String inMsg = msg.replaceAll("\\s+", "").trim().toUpperCase();
        StringBuilder result = new StringBuilder(inMsg.length());
        for (int i = 0; i < inMsg.length(); i++) {
            int converted = convert(_alphabet.toInt(inMsg.charAt(i)));
            result.append(_alphabet.toChar(converted));
        }
        return result.toString().trim();
    }

    /** Common alphabet of my rotors. */
    private final Alphabet _alphabet;

    /** Total number of rotors in the machine. */
    private int _numRotors;

    /** Number of pawls in the machine. */
    private int _pawls;

    /** First permutation for the input message. */
    private Permutation _plugboard;

    /** ArrayList of all rotors in the machine. */
    private ArrayList<Rotor> _allRotors;

    /** ArrayList of all active rotors in the machine. */
    private ArrayList<Rotor> _activeRotors;
}
