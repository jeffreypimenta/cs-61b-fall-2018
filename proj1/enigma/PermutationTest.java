package enigma;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.Timeout;
import static org.junit.Assert.*;
import java.util.HashMap;

import static enigma.TestUtils.*;

/** The suite of all JUnit tests for the Permutation class.
 *  @author J. S. Pimenta
 */
public class PermutationTest {

    /** Testing time limit. */
    @Rule
    public Timeout globalTimeout = Timeout.seconds(5);

    /* ***** TESTING UTILITIES ***** */

    private Permutation perm;
    private String alpha = UPPER_STRING;

    /** Check that perm has an alphabet whose size is that of
     *  FROMALPHA and TOALPHA and that maps each character of
     *  FROMALPHA to the corresponding character of FROMALPHA, and
     *  vice-versa. TESTID is used in error messages. */
    private void checkPerm(String testId,
                           String fromAlpha, String toAlpha) {
        int N = fromAlpha.length();
        assertEquals(testId + " (wrong length)", N, perm.size());
        for (int i = 0; i < N; i += 1) {
            char c = fromAlpha.charAt(i), e = toAlpha.charAt(i);
            assertEquals(msg(testId, "wrong translation of '%c'", c),
                         e, perm.permute(c));
            assertEquals(msg(testId, "wrong inverse of '%c'", e),
                         c, perm.invert(e));
            int ci = alpha.indexOf(c), ei = alpha.indexOf(e);
            assertEquals(msg(testId, "wrong translation of %d", ci),
                         ei, perm.permute(ci));
            assertEquals(msg(testId, "wrong inverse of %d", ei),
                         ci, perm.invert(ei));
        }
    }

    /* ***** TESTS ***** */

    @Test
    public void checkIdTransform() {
        perm = new Permutation("", UPPER);
        checkPerm("identity", UPPER_STRING, UPPER_STRING);
    }

    @Test
    public void checkPermNAVALA() {
        for (HashMap.Entry<String, String> pair : TestUtils.NAVALA.entrySet()) {
            if (pair.getKey().equals("B") || pair.getKey().equals("C")) {
                continue;
            }
            perm = new Permutation(pair.getValue(), UPPER);
            String expected = TestUtils.NAVALA_MAP.get(pair.getKey());
            checkPerm(pair.getKey(), UPPER_STRING, expected);
        }
    }

    @Test
    public void checkPermNAVALB() {
        for (HashMap.Entry<String, String> pair : TestUtils.NAVALB.entrySet()) {
            if (pair.getKey().equals("B") || pair.getKey().equals("C")) {
                continue;
            }
            perm = new Permutation(pair.getValue(), UPPER);
            String expected = TestUtils.NAVALB_MAP.get(pair.getKey());
            checkPerm(pair.getKey(), UPPER_STRING, expected);
        }
    }

    @Test
    public void checkPermNAVALZ() {
        for (HashMap.Entry<String, String> pair : TestUtils.NAVALZ.entrySet()) {
            if (pair.getKey().equals("B") || pair.getKey().equals("C")) {
                continue;
            }
            perm = new Permutation(pair.getValue(), UPPER);
            String expected = TestUtils.NAVALZ_MAP.get(pair.getKey());
            checkPerm(pair.getKey(), UPPER_STRING, expected);
        }
    }

}
