package enigma;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

import static enigma.EnigmaException.*;

/** Enigma simulator.
 *  @author J. S. Pimenta
 */
public final class Main {

    /** Process a sequence of encryptions and decryptions, as
     *  specified by ARGS, where 1 <= ARGS.length <= 3.
     *  ARGS[0] is the name of a configuration file.
     *  ARGS[1] is optional; when present, it names an input file
     *  containing messages.  Otherwise, input comes from the standard
     *  input.  ARGS[2] is optional; when present, it names an output
     *  file for processed messages.  Otherwise, output goes to the
     *  standard output. Exits normally if there are no errors in the input;
     *  otherwise with code 1. */
    public static void main(String... args) {
        try {
            new Main(args).process();
            return;
        } catch (EnigmaException excp) {
            System.err.printf("Error: %s%n", excp.getMessage());
        }
        System.exit(1);
    }

    /** Check ARGS and open the necessary files (see comment on main). */
    Main(String[] args) {
        if (args.length < 1 || args.length > 3) {
            throw error("Only 1, 2, or 3 command-line arguments allowed");
        }

        _config = getInput(args[0]);

        if (args.length > 1) {
            _input = getInput(args[1]);
        } else {
            _input = new Scanner(System.in);
        }

        if (args.length > 2) {
            _output = getOutput(args[2]);
        } else {
            _output = System.out;
        }
    }

    /** Return a Scanner reading from the file named NAME. */
    private Scanner getInput(String name) {
        try {
            return new Scanner(new File(name));
        } catch (IOException excp) {
            throw error("could not open %s", name);
        }
    }

    /** Return a PrintStream writing to the file named NAME. */
    private PrintStream getOutput(String name) {
        try {
            return new PrintStream(new File(name));
        } catch (IOException excp) {
            throw error("could not open %s", name);
        }
    }

    /** Configure an Enigma machine from the contents of configuration
     *  file _config and apply it to the messages in _input, sending the
     *  results to _output. */
    private void process() {
        Machine enigma = readConfig();
        while (_input.hasNextLine()) {
            String inStr = _input.nextLine().trim();
            if (inStr.isEmpty()) {
                _output.print("\n");
                continue;
            }
            if (inStr.charAt(0) == '*') {
                if (!inStr.substring(1).trim().matches("[a-zA-Z0-9()\\s]+")) {
                    throw error("Invalid alphabet on setting line");
                }
                setUp(enigma, inStr.trim());
                continue;
            }
            if (!enigma.isSetUp()) {
                throw error("Missing or invalid setting line "
                        + "before start of message.");
            }
            String outStr = enigma.convert(inStr);
            printMessageLine(outStr);
        }
        _output.close();
    }

    /** Checks whether the string on line 1 is a valid alphabet.
     * @param s is the input string. */
    private void checkLine1(String s) {
        if (!s.matches("[a-zA-Z0-9-]+") || s.isEmpty() || s.length() < 3) {
            throw error("Invalid alphabet on "
                    + "line 1  of config file");
        }
    }

    /** Return an Enigma machine configured from the contents of configuration
     *  file _config. */
    private Machine readConfig() {
        try {
            if (_config.hasNextLine()) {
                String line1 = _config.nextLine().trim();
                checkLine1(line1);
                _alphabet = new CharacterRange(line1);
            }
            int numRotors = 0, numPawls = 0;
            if (_config.hasNextLine()) {
                String[] line2 = _config.nextLine().trim().split(" ");
                if (line2[0].isEmpty() || line2.length != 2
                        || !line2[0].matches("[0-9]+")
                        || !line2[1].matches("[0-9]+")) {
                    throw error("Invalid characters on line2");
                }
                numRotors = Integer.parseInt(line2[0]);
                numPawls = Integer.parseInt(line2[1]);
                if (numRotors <= numPawls || numPawls < 1) {
                    throw error("Invalid rotor and pawl combination");
                }
            }
            String[] str = new String[_configSize];
            int lineNum = 0;
            while (_config.hasNextLine()) {
                str[lineNum] = _config.nextLine().trim();
                if (str[lineNum].isEmpty()) {
                    continue;
                }
                if (!str[lineNum].matches("[a-zA-Z0-9()\\s]+")) {
                    throw error("Invalid alphabet on line %s", lineNum + 3);
                }
                if (str[lineNum].charAt(0) == '(') {
                    StringBuilder temp = new StringBuilder(str[lineNum - 1]);
                    temp.append(" " + str[lineNum]);
                    str[lineNum - 1] = temp.toString();
                    continue;
                }
                lineNum++;
            }
            if (lineNum < numRotors) {
                throw error("Insufficient number of "
                        + "rotor specification lines");
            }
            StringBuilder configStr = new StringBuilder();
            for (int i = 0; i < lineNum; i++) {
                configStr.append(str[i] + "\n");
            }
            _config = new Scanner(configStr.toString());
            ArrayList<Rotor> allRotors = new ArrayList<>();
            while (_config.hasNextLine()) {
                allRotors.add(readRotor());
            }
            return new Machine(_alphabet, numRotors, numPawls, allRotors);
        } catch (NoSuchElementException excp) {
            throw error("configuration file truncated");
        }
    }

    /** Checks whether a String has duplicate characters.
     * @param s is the input string.
     * @return true if there are duplicate characters in s. */
    private boolean duplicateChars(String s) {
        char[] chars = s.toCharArray();
        Map<Character, Integer> charm = new HashMap<>();
        for (Character c: chars) {
            if (charm.containsKey(c)) {
                return true;
            } else {
                charm.put(c, 1);
            }
        }
        return false;
    }

    /** Return a rotor, reading its description from _config. */
    private Rotor readRotor() {
        try {
            Rotor r;
            String name, notches;
            char type;
            String str = _config.nextLine().toUpperCase();
            String str1 = str.substring(0, str.indexOf('('));
            String cycles = str.substring(str.indexOf('('));
            if (duplicateChars(cycles.replaceAll("[)\\s(]", "").trim())) {
                throw error("Duplicate characters in cycle f"
                        + "or this rotor");
            }
            if (!cycles.matches("(\\([A-Z0-9]+\\)\\s*)+(\\([A-Z0-9]+\\))*")) {
                throw error("Invalid cycles specification for rotor");
            }
            String[] rotorStr;
            rotorStr = str1.trim().split(" ");
            if (rotorStr.length != 2) {
                throw error("Invalid rotor specification");
            }
            name = rotorStr[0];
            type = rotorStr[1].charAt(0);
            notches = rotorStr[1].substring(1);
            if (!notches.matches("^[a-zA-Z0-9]{1,2}") && !notches.isEmpty()) {
                throw error("Invalid notch in config file");
            }
            Permutation added = new Permutation(cycles, _alphabet);
            switch (type) {
            case 'M': r = new MovingRotor(name, added, notches);
                        break;
            case 'N': r = new FixedRotor(name, added);
                if (!notches.isEmpty()) {
                    throw error("Fixed rotors cannot have notches");
                }
                    break;
            case 'R': r = new Reflector(name, added);
                if (!notches.isEmpty()) {
                    throw error("Reflectors cannot have notches");
                }
                    break;
            default: throw error("Invalid rotor type %c", type);
            }
            return r;
        } catch (NoSuchElementException excp) {
            throw error("bad rotor description");
        }
    }

    /** Set M according to the specification given on SETTINGS,
     *  which must have the format specified in the assignment. */
    private void setUp(Machine M, String settings) {
        String setStr = settings.substring(1).trim().toUpperCase();
        int pIndex = setStr.indexOf('(');
        String rotSet, plugboard;
        if (pIndex == -1) {
            plugboard = "";
            rotSet = setStr;
        } else {
            rotSet = setStr.substring(0, pIndex);
            plugboard = setStr.substring(pIndex);
            if (!plugboard.matches
                    ("(\\([A-Z0-9]+\\)\\s*)+(\\([A-Z0-9]+\\))*")) {
                throw error("Invalid plugboard specifications");
            }
            if (duplicateChars(plugboard.replaceAll("[)\\s(]", "").trim())) {
                throw error("Multiple assignment in plugboard");
            }
        }
        String[] rotorOrder = rotSet.split(" ");
        String setting = rotorOrder[rotorOrder.length - 1];
        String[] rotors = new String[rotorOrder.length - 1];
        System.arraycopy(rotorOrder, 0, rotors, 0, rotors.length);
        if (rotors.length != M.numRotors()) {
            throw error("Incorrect number of rotors on the setting line");
        }
        M.insertRotors(rotors);
        M.setRotors(setting);
        M.setPlugboard(new Permutation(plugboard, _alphabet));
    }

    /** Print MSG in groups of five (except that the last group may
     *  have fewer letters). */
    private void printMessageLine(String msg) {
        int capacity = msg.length() + (msg.length() / 5);
        StringBuilder result = new StringBuilder(capacity);
        for (int i = 0; i < msg.length(); i++) {
            result.append(msg.charAt(i));
            if ((i + 1) % 5 == 0 && i != msg.length() - 1) {
                result.append(' ');
            }
        }
        _output.println(result.toString());
    }

    /** Alphabet used in this machine. */
    private Alphabet _alphabet;

    /** Source of input messages. */
    private Scanner _input;

    /** Source of machine configuration. */
    private Scanner _config;

    /** File for encoded/decoded messages. */
    private PrintStream _output;

    /** Size of the config. */
    private final int _configSize = 50;

}
