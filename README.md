This repository contains all project work pertaining to the CS 61B Data Structures course at the University of California, Berkeley.

Here is a key of each subdirectory and the project it contains:

proj0 --> "Galaxies", a 2D-tile interactive game mimicking Simon Tatham's game of the same name (https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/galaxies.html)

proj1 --> "Enigma", an imitation of the German Enigma machine used in World War II

proj2 --> "Amazons", an Argentinian board game resembling chess that uses AI capable of winning in within 10 moves

proj3 --> "Fun with Graphs", two clients of a Graph package that (1) mimic GNU make, which tracks and controls the generation of executables from a source file, and (2) resemble a GPS mapping application that takes in a map and a request, outputting a series of directions to the desired destination.