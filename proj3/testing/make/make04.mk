edit: main.o kbd.o command.o display.o
    cc -o edit main.o kbd.o command.o display.o

main.o: main.c
    cc -c main.c

main.o: defs.h

kbd.o: kbd.c defs.h command.h
    cc -c kbd.c

command.o: command.c defs.h command.h
    cc -c command.c

display.o: display.c defs.h
    cc -c display.c
