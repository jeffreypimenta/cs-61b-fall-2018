package graph;

/* See restrictions in Graph.java. */

import java.util.ArrayList;

/** Represents a general unlabeled directed graph whose vertices are denoted by
 *  positive integers. Graphs may have self edges.
 *
 *  @author J. S. Pimenta
 */
public class DirectedGraph extends GraphObj {

    @Override
    public boolean isDirected() {
        return true;
    }

    @Override
    public int inDegree(int v) {
        if (!contains(v)) {
            return 0;
        }
        return _revAdjList[v].size();
    }

    @Override
    public Iteration<Integer> predecessors(int v) {
        ArrayList<Integer> predecessor = new ArrayList<>();
        if (_revAdjList[v] != null) {
            for (Integer i : _revAdjList[v]) {
                predecessor.add(_edge.get(i).getFrom());
            }
        }
        return Iteration.iteration(predecessor.iterator());
    }
}
