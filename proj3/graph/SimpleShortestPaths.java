package graph;

/* See restrictions in Graph.java. */

/** A partial implementation of ShortestPaths that contains the weights of
 *  the vertices and the predecessor edges.   The client needs to
 *  supply only the two-argument getWeight method.
 *  @author J. S. Pimenta
 */
public abstract class SimpleShortestPaths extends ShortestPaths {

    /** The shortest paths in G from SOURCE. */
    public SimpleShortestPaths(Graph G, int source) {
        this(G, source, 0);
    }

    /** A shortest path in G from SOURCE to DEST. */
    public SimpleShortestPaths(Graph G, int source, int dest) {
        super(G, source, dest);
        _predecessor = new Integer[G.maxVertex() + 1];
        _weight = new Double[G.maxVertex() + 1];
        for (Integer v : G.vertices()) {
            _weight[v] = Double.MAX_VALUE;
            _predecessor[v] = 0;
        }
    }

    /** Returns the current weight of edge (U, V) in the graph.  If (U, V) is
     *  not in the graph, returns positive infinity. */
    @Override
    protected abstract double getWeight(int u, int v);

    @Override
    public double getWeight(int v) {
        if (_G.contains(v)) {
            return _weight[v];
        } else {
            return Double.MAX_VALUE;
        }
    }

    @Override
    protected void setWeight(int v, double w) {
        _weight[v] = w;
    }

    @Override
    public int getPredecessor(int v) {
        return _predecessor[v];
    }

    @Override
    protected void setPredecessor(int v, int u) {
        _predecessor[v] = u;
    }

    /** List of predecessors of a node.*/
    protected Integer[] _predecessor;
    /** List of weights of a graph. */
    protected Double[] _weight;
}
