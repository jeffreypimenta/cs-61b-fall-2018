package graph;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

/** unit tests for paths
 * @author J. S. Pimenta
 */
public class PathsTesting {

    class GraphPathTest extends SimpleShortestPaths {
        private GraphPathTest(Graph g, int source, int target) {
            super(g, source, target);
            _G = g;
            eDist = new Double[g.maxVertex() + 1];
            eWt = new Double[g.edgeSize() + 1];
            setWt(g.edgeId(4, 5), 2.0);
            setWt(g.edgeId(5, 3), 1.0);
            setWt(g.edgeId(5, 6), 4.0);
            setWt(g.edgeId(5, 7), 5.0);
            setWt(g.edgeId(7, 4), 1.0);
            setWt(g.edgeId(7, 6), 1.0);
            setWt(g.edgeId(1, 2), 2.0);
            setWt(g.edgeId(1, 3), 1.0);
            setWt(g.edgeId(2, 3), 5.0);
            setWt(g.edgeId(2, 4), 9.0);
            setWt(g.edgeId(2, 5), 3.0);
            setWt(g.edgeId(3, 6), 14.0);
            setDistance(1, 0.0);
            setDistance(2, 1.5);
            setDistance(3, 0.65);
            setDistance(4, 11.3);
            setDistance(5, 4.0);
            setDistance(6, 9.0);
            setDistance(7, 7.0);
        }

        private void setWt(Integer id, double wt) {
            eWt[id] = wt;
        }

        private void setDistance(Integer v, double dist) {
            eDist[v] = dist;
        }

        @Override
        protected double getWeight(int u, int v) {
            Double wt = eWt[_G.edgeId(u, v)];
            if (wt > 0) {
                return wt;
            } else {
                return Double.MAX_VALUE;
            }
        }

        @Override
        protected double estimatedDistance(int v) {
            return eDist[v];
        }

        private Double[] eWt;
        private Double[] eDist;
        Graph _G;
    }


    @Test
    public void testWeights() {
        Graph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        g.add();
        g.add();
        g.add();
        g.add();
        g.add(2, 4);
        g.add(2, 5);
        g.add(3, 6);
        g.add(4, 5);
        g.add(5, 3);
        g.add(5, 6);
        g.add(5, 7);
        g.add(7, 4);
        g.add(7, 6);
        GraphPathTest gp = new GraphPathTest(g, 1, 7);
        gp.setPaths();
        List<Integer> path = gp.pathTo();
        Iterator<Integer> it = path.iterator();
        assertEquals(1, it.next().intValue());
        assertEquals(2, it.next().intValue());
        assertEquals(5, it.next().intValue());
        assertEquals(7, it.next().intValue());
    }

    @Test
    public void testWeights2() {
        Graph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        g.add();
        g.add();
        g.add();
        g.add();
        g.add(2, 4);
        g.add(2, 5);
        g.add(3, 6);
        g.add(4, 5);
        g.add(5, 3);
        g.add(5, 6);
        g.add(5, 7);
        g.add(7, 4);
        g.add(7, 6);
        GraphPathTest gp2 = new GraphPathTest(g, 1, 4);
        gp2.setPaths();
        List<Integer> path2 = gp2.pathTo();
        Iterator<Integer> it2 = path2.iterator();
        assertEquals(1, it2.next().intValue());
        assertEquals(2, it2.next().intValue());
        assertEquals(4, it2.next().intValue());
    }
}
