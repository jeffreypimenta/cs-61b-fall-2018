package graph;

/* See restrictions in Graph.java. */


import java.util.AbstractQueue;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/** The shortest paths through an edge-weighted graph.
 *  By overrriding methods getWeight, setWeight, getPredecessor, and
 *  setPredecessor, the client can determine how to represent the weighting
 *  and the search results.  By overriding estimatedDistance, clients
 *  can search for paths to specific destinations using A* search.
 *  @author J. S. Pimenta
 */
public abstract class ShortestPaths {

    /** Subclass of Traversal for Shortest Paths. */
    class ShortestPathTraversal extends Traversal {

        /** Constructor for SPT.
         * @param G is the input graph.
         * @param fringe is the queue of vertices. */
        ShortestPathTraversal(Graph G, Queue<Integer> fringe) {
            super(G, fringe);
        }

        @Override
        protected boolean visit(int v) {
            if (v == ShortestPaths.this.getDest()) {
                return false;
            }
            for (Integer w : _G.successors(v)) {
                double edgeWeight = getWeight(v, w);
                double distW = getWeight(w);
                double distV = getWeight(v);
                double newDist = distV + edgeWeight;
                if (distW > newDist) {
                    setWeight(w, newDist);
                    setPredecessor(w, v);
                }
            }
            return true;
        }


    }

    /** Class for Shortest Paths Queue. */
    class SPQueue extends AbstractQueue<Integer> {
        /** Priority queue node. */
        class PQNode {
            /** Constructor for PQNode.
             * @param v vertex
             * @param val value*/
            PQNode(int v, double val) {
                this._vertex = v;
                this._value = val;
            }

            /** Vertex of the PQNode.
             * @return vertex. */
            int getVertex() {
                return _vertex;
            }

            /** Value of the PQNode.
             * @return value. */
            double getValue() {
                return _value;
            }

            /** Vertex of PQNode.*/
            private int _vertex;
            /** Value of PQNode.*/
            private double _value;
        }

        /** Comparator for PQNodes. */
        class PQNodeComparator implements Comparator<PQNode> {
            @Override
            public int compare(PQNode n1, PQNode n2) {
                if (n1.getValue() == n2.getValue()) {
                    return 0;
                } else if (n1.getValue() < n2.getValue()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }

        /** Constructor for SPQueue. */
        SPQueue() {
            _pq = new PriorityQueue<>(1, new PQNodeComparator());
            _nodeIndex = new ArrayList<>();
        }

        @Override
        public boolean offer(Integer e) {
            _nodeIndex.add(e);
            return _pq.offer(new PQNode(e,
                    getWeight(e) + estimatedDistance(e)));
        }

        @Override
        public Integer poll() {
            PQNode v = _pq.poll();
            if (v == null) {
                return null;
            } else {
                Integer val = v.getVertex();
                _nodeIndex.remove(val);
                return val;
            }
        }

        @Override
        public Integer peek() {
            PQNode v = _pq.peek();
            if (v == null) {
                return null;
            } else {
                return v.getVertex();
            }
        }

        @Override
        public Iterator<Integer> iterator() {
            return _nodeIndex.iterator();
        }

        @Override
        public int size() {
            return _pq.size();
        }

        /** Priority queue for PQNodes.*/
        private PriorityQueue<PQNode> _pq;
        /** List of node indices. */
        private ArrayList<Integer> _nodeIndex;

    }

    /** The shortest paths in G from SOURCE. */
    public ShortestPaths(Graph G, int source) {
        this(G, source, 0);
    }

    /** A shortest path in G from SOURCE to DEST. */
    public ShortestPaths(Graph G, int source, int dest) {
        _G = G;
        _source = source;
        _dest = dest;
        _path = new LinkedList<>();
        _spTraversal = new ShortestPathTraversal(G, new SPQueue());
    }

    /** Initialize the shortest paths.  Must be called before using
     *  getWeight, getPredecessor, and pathTo. */
    public void setPaths() {
        for (Integer i : _G.vertices()) {
            setWeight(i, Double.MAX_VALUE);
            setPredecessor(i, 0);
        }
        setWeight(getSource(), 0);
        _spTraversal.traverse(getSource());
    }

    /** Returns the starting vertex. */
    public int getSource() {
        return _source;
    }

    /** Returns the target vertex, or 0 if there is none. */
    public int getDest() {
        return _dest;
    }

    /** Returns the current weight of vertex V in the graph.  If V is
     *  not in the graph, returns positive infinity. */
    public abstract double getWeight(int v);

    /** Set getWeight(V) to W. Assumes V is in the graph. */
    protected abstract void setWeight(int v, double w);

    /** Returns the current predecessor vertex of vertex V in the graph, or 0 if
     *  V is not in the graph or has no predecessor. */
    public abstract int getPredecessor(int v);

    /** Set getPredecessor(V) to U. */
    protected abstract void setPredecessor(int v, int u);

    /** Returns an estimated heuristic weight of the shortest path from vertex
     *  V to the destination vertex (if any).  This is assumed to be less
     *  than the actual weight, and is 0 by default. */
    protected double estimatedDistance(int v) {
        return 0.0;
    }

    /** Returns the current weight of edge (U, V) in the graph.  If (U, V) is
     *  not in the graph, returns positive infinity. */
    protected abstract double getWeight(int u, int v);

    /** Returns a list of vertices starting at _source and ending
     *  at V that represents a shortest path to V.  Invalid if there is a
     *  destination vertex other than V. */
    public List<Integer> pathTo(int v) {
        while (v != getSource()) {
            _path.addFirst(v);
            v = getPredecessor(v);
        }
        _path.addFirst(getSource());
        return _path;
    }

    /** Returns a list of vertices starting at the source and ending at the
     *  destination vertex. Invalid if the destination is not specified. */
    public List<Integer> pathTo() {
        return pathTo(getDest());
    }
    /** The graph being searched. */
    protected final Graph _G;
    /** The starting vertex. */
    private final int _source;
    /** The target vertex. */
    private final int _dest;
    /** Shotest paths traversal variable. */
    private ShortestPathTraversal _spTraversal;
    /** Linked list representing a path. */
    private LinkedList<Integer> _path;
}
