package graph;

/* See restrictions in Graph.java. */

import java.util.ArrayDeque;

/** Implements a breadth-first traversal of a graph.  Generally, the
 *  client will extend this class, overriding the visit method as desired
 *  (by default, it does nothing).
 *  @author J. S. Pimenta
 */
public class BreadthFirstTraversal extends Traversal {

    /** A breadth-first Traversal of G. */
    protected BreadthFirstTraversal(Graph G) {
        super(G, _deque);
    }

    @Override
    protected boolean visit(int v) {
        return super.visit(v);
    }

    /** Deque to input in constructor. */
    private static ArrayDeque<Integer> _deque = new ArrayDeque<>();

}
