package graph;

/* See restrictions in Graph.java. */

import java.util.ArrayList;

/** A partial implementation of Graph containing elements common to
 *  directed and undirected graphs.
 *
 *  @author J. S. Pimenta
 */
abstract class GraphObj extends Graph {

    /** A new, empty Graph. */
    @SuppressWarnings("unchecked")
    GraphObj() {
        _edge = new ArrayList<>();
        _edge.add(new Edge(-1, -1));
        _adjList = (ArrayList<Integer>[]) new ArrayList[MAXNODES];
        _revAdjList = (ArrayList<Integer>[]) new ArrayList[MAXNODES];
        _maxVertex = 0;
        _vertexCount = 0;
        _edgeCount = 0;
    }

    @Override
    public int vertexSize() {
        return _vertexCount;
    }

    @Override
    public int maxVertex() {
        return _maxVertex;
    }

    @Override
    public int edgeSize() {
        return _edgeCount;
    }

    @Override
    public abstract boolean isDirected();

    @Override
    public int outDegree(int v) {
        if (!contains(v)) {
            return 0;
        }
        return _adjList[v].size();
    }

    @Override
    public abstract int inDegree(int v);

    @Override
    public boolean contains(int u) {
        return _adjList[u] != null;
    }

    @Override
    public boolean contains(int u, int v) {
        if (!contains(u) || !contains(v)) {
            return false;
        }
        return _adjList[u].contains(edgeId(u, v));
    }

    @Override
    public int add() {
        int i;
        for (i = 1; i <= maxVertex(); i++) {
            if (_adjList[i] == null) {
                break;
            }
        }
        _adjList[i] = new ArrayList<>();
        if (isDirected()) {
            _revAdjList[i] = new ArrayList<>();
        }
        _vertexCount++;
        if (i == (_maxVertex + 1)) {
            _maxVertex++;
        }
        return i;
    }

    @Override
    public int add(int u, int v) {
        checkMyVertex(u);
        checkMyVertex(v);
        if (contains(u, v)) {
            return edgeId(u, v);
        }
        _edge.add(new Edge(u, v));
        int edgeIndex = _edge.size() - 1;
        if (isDirected()) {
            _adjList[u].add(edgeIndex);
            _revAdjList[v].add(edgeIndex);
        } else {
            _adjList[u].add(edgeIndex);
            if (u != v) {
                _adjList[v].add(edgeIndex);
            }
        }
        _edgeCount++;
        return edgeIndex;
    }

    /** Deletes edge at an index.
     * @param i is index. */
    private void delEdge(int i) {
        if (_edge.get(i).getFrom() != -1) {
            _edge.set(i, new Edge(-1, -1));
            _edgeCount--;
        }
    }

    @Override
    public void remove(int v) {
        if (!contains(v)) {
            return;
        }
        ArrayList<Integer> delEdges = new ArrayList<>();
        for (Integer i : _adjList[v]) {
            delEdges.add(i);
        }
        if (isDirected()) {
            for (Integer i : _revAdjList[v]) {
                delEdges.add(i);
            }
        }
        if (isDirected()) {
            for (Integer s : successors(v)) {
                if (s == v) {
                    continue;
                }
                for (Integer eIndex : delEdges) {
                    if (_revAdjList[s].contains(eIndex)) {
                        _revAdjList[s].remove(eIndex);
                    }
                }
            }
        }
        for (Integer p : predecessors(v)) {
            if (p == v) {
                continue;
            }
            for (Integer eIndex : delEdges) {
                if (_adjList[p].contains(eIndex)) {
                    _adjList[p].remove(eIndex);
                }
            }
        }
        for (Integer i : _adjList[v]) {
            delEdge(i);
        }
        if (isDirected()) {
            for (Integer i : _revAdjList[v]) {
                delEdge(i);
            }
        }
        _adjList[v] = null;
        if (isDirected()) {
            _revAdjList[v] = null;
        }
        _vertexCount--;
        if (v == maxVertex()) {
            _maxVertex--;
        }
    }

    @Override
    public void remove(int u, int v) {
        if (!contains(u, v)) {
            return;
        }
        int i = edgeId(u, v);
        if (_edge.get(i).getFrom() != -1) {
            _edge.set(i, new Edge(-1, -1));
            _edgeCount--;
        }
        _adjList[u].remove((Integer) i);
        if (isDirected()) {
            _revAdjList[v].remove((Integer) i);
        } else {
            _adjList[v].remove((Integer) i);
        }
    }

    @Override
    public Iteration<Integer> vertices() {
        ArrayList<Integer> vert = new ArrayList<>();
        for (int i = 1; i < maxVertex() + 1; i++) {
            if (_adjList[i] == null) {
                continue;
            }
            vert.add(i);
        }
        return Iteration.iteration(vert.iterator());
    }

    @Override
    public Iteration<Integer> successors(int v) {
        ArrayList<Integer> successor = new ArrayList<>();
        if (_adjList[v] != null) {
            for (Integer i : _adjList[v]) {
                Edge e = _edge.get(i);
                if (isDirected()) {
                    successor.add(e.getTo());
                } else {
                    if (e.getTo() == v) {
                        successor.add(e.getFrom());
                    } else {
                        successor.add(e.getTo());
                    }
                }
            }
        }
        return Iteration.iteration(successor.iterator());
    }

    @Override
    public abstract Iteration<Integer> predecessors(int v);

    @Override
    public Iteration<int[]> edges() {
        ArrayList<int[]> edgeArr = new ArrayList<>();
        for (Edge e : _edge) {
            if (e.getFrom() == -1) {
                continue;
            }
            int[] edge = new int[2];
            edge[0] = e.getFrom();
            edge[1] = e.getTo();
            edgeArr.add(edge);
        }
        return Iteration.iteration(edgeArr.iterator());
    }

    @Override
    protected void checkMyVertex(int v) {
        if (!contains(v)) {
            throw new IllegalArgumentException("vertex not from Graph");
        }
    }

    @Override
    protected int edgeId(int u, int v) {
        for (int i = 1; i < _edge.size(); i++) {
            Edge e = _edge.get(i);
            if (isDirected()) {
                if (e.getFrom() == u && e.getTo() == v) {
                    return i;
                }
            } else {
                if ((e.getFrom() == u && e.getTo() == v)
                        || (e.getTo() == u && e.getFrom() == v)) {
                    return i;
                }
            }
        }
        return 0;
    }

    /** Nested class for edges. */
    protected class Edge {
        /** Constructor for edge class.
         * @param f from vertex.
         * @param t to vertex.*/
        Edge(int f, int t) {
            from = f;
            to = t;
        }

        /** Returns "from" vertex of an edge. */
        int getFrom() {
            return from;
        }

        /** Returns "to" vertex of an edge. */
        int getTo() {
            return to;
        }

        /** Vertex that edge comes from. */
        private int from;
        /** Vertex that edge goes to. */
        private int to;
    }

    /** List of edges. */
    protected ArrayList<Edge> _edge;
    /** Array of adjacency lists. */
    protected ArrayList<Integer>[] _adjList;
    /** Array of reverse adjacency lists. */
    protected ArrayList<Integer>[] _revAdjList;
    /** Highest index of non-null vertex in array. */
    private int _maxVertex;
    /** Total count of vertices. */
    private int _vertexCount;
    /** Total count of edges. */
    private int _edgeCount;
    /** Standard amount of maximum nodes in array. */
    private static final int MAXNODES = 10000;
}
