package graph;

import org.junit.Test;
import static org.junit.Assert.*;

/** Unit tests for the Graph class.
 *  @author J. S. Pimenta
 */
public class GraphTest {

    @Test
    public void emptyGraph() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.vertexSize());
        assertEquals(0, g.edgeSize());

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.vertexSize());
        assertEquals(0, u.edgeSize());
    }

    @Test
    public void testGraphAdds() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(1, g.add());
        assertEquals(2, g.add());
        assertEquals(3, g.add());
        assertEquals(1, g.add(1, 2));
        assertEquals(2, g.add(2, 3));
        assertEquals(1, g.add(1, 2));
        assertEquals(4, g.add());

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(1, u.add());
        assertEquals(2, u.add());
        assertEquals(3, u.add());
        assertEquals(1, u.add(1, 2));
        assertEquals(2, u.add(2, 3));
        assertEquals(1, u.add(1, 2));
        assertEquals(4, u.add());
    }

    @Test
    public void testGraphContains() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        assertTrue(g.contains(1));
        assertFalse(g.contains(2));
        g.add();
        assertTrue(g.contains(2));
        g.add();
        g.add(1, 2);
        g.add(2, 3);
        assertTrue(g.contains(1, 2));
        assertFalse(g.contains(2, 1));
        assertTrue(g.contains(2, 3));
        assertFalse(g.contains(3, 2));

        UndirectedGraph u = new UndirectedGraph();
        u.add();
        assertTrue(u.contains(1));
        assertFalse(u.contains(2));
        u.add();
        assertTrue(u.contains(2));
        u.add();
        u.add(1, 2);
        u.add(2, 3);
        assertTrue(u.contains(1, 2));
        assertTrue(u.contains(2, 1));
        assertTrue(u.contains(2, 3));
        assertFalse(u.contains(1, 3));
    }

    @Test
    public void testVertexSize() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.vertexSize());
        g.add();
        g.add();
        assertEquals(2, g.vertexSize());

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.vertexSize());
        u.add();
        u.add();
        assertEquals(2, u.vertexSize());
    }

    @Test
    public void testMaxVertex() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.maxVertex());
        g.add();
        g.add();
        g.add();
        assertEquals(3, g.maxVertex());
        g.add();
        assertEquals(4, g.maxVertex());

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.maxVertex());
        u.add();
        u.add();
        u.add();
        assertEquals(3, u.maxVertex());
        u.add();
        assertEquals(4, u.maxVertex());
    }

    @Test
    public void testEdgeSize() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.edgeSize());
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(2, 3);
        assertEquals(2, g.edgeSize());

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.edgeSize());
        u.add();
        u.add();
        u.add();
        u.add(1, 2);
        u.add(2, 3);
        assertEquals(2, u.edgeSize());
    }

    @Test
    public void testIsDirected() {
        DirectedGraph g = new DirectedGraph();
        assertTrue(g.isDirected());

        UndirectedGraph u = new UndirectedGraph();
        assertFalse(u.isDirected());
    }

    @Test
    public void testOutDegree() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.outDegree(1));
        g.add();
        g.add();
        g.add();
        assertEquals(0, g.outDegree(2));
        g.add(1, 2);
        g.add(1, 3);
        assertEquals(2, g.outDegree(1));
        assertEquals(0, g.outDegree(3));
        g.add(3, 1);
        assertEquals(1, g.outDegree(3));
    }

    @Test
    public void testInDegree() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.inDegree(1));
        g.add();
        g.add();
        g.add();
        assertEquals(0, g.inDegree(2));
        g.add(1, 2);
        g.add(1, 3);
        assertEquals(0, g.inDegree(1));
        assertEquals(1, g.inDegree(3));
        g.add(3, 1);
        assertEquals(1, g.inDegree(1));
        g.add(2, 1);
        assertEquals(2, g.inDegree(1));
    }

    @Test
    public void testDegree() {
        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.degree(1));
        u.add();
        u.add();
        u.add();
        assertEquals(0, u.degree(2));
        u.add(1, 2);
        u.add(1, 3);
        assertEquals(2, u.degree(1));
        assertEquals(1, u.degree(3));
        u.add(3, 1);
        assertEquals(1, u.degree(3));
    }

    @Test
    public void testRemove() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        assertTrue(g.contains(1));
        g.remove(1);
        assertFalse(g.contains(1));
        g.add();
        assertTrue(g.contains(1));
        g.add(1, 2);
        assertTrue(g.contains(1, 2));
        g.remove(1);
        assertFalse(g.contains(1, 2));

        UndirectedGraph u = new UndirectedGraph();
        u.add();
        u.add();
        assertTrue(u.contains(1));
        u.remove(1);
        assertFalse(u.contains(1));
        u.add();
        assertTrue(u.contains(1));
        u.add(1, 2);
        assertTrue(u.contains(1, 2));
        u.remove(1);
        assertFalse(u.contains(1, 2));
        u.add();
        u.add(1, 2);
        assertTrue(u.contains(1, 2));
        u.remove(1, 2);
        assertFalse(u.contains(1, 2));
    }

    @Test
    public void testVertices() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        Iteration<Integer> it = g.vertices();
        int count = 1;
        while (it.hasNext()) {
            assertEquals(count, it.next().intValue());
            count++;
        }

        UndirectedGraph u = new UndirectedGraph();
        u.add();
        u.add();
        u.add();
        it = u.vertices();
        count = 1;
        while (it.hasNext()) {
            assertEquals(count, it.next().intValue());
            count++;
        }
    }

    @Test
    public void testSuccessors() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        g.add(2, 4);
        Iteration<Integer> it = g.successors(1);
        assertEquals(2, it.next().intValue());
        assertEquals(3, it.next().intValue());
        it = g.successors(2);
        assertEquals(3, it.next().intValue());
        assertEquals(4, it.next().intValue());
    }

    @Test
    public void testPredecessors() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add();
        g.add(3, 2);
        g.add(1, 3);
        g.add(1, 2);
        g.add(2, 3);
        g.add(2, 4);
        Iteration<Integer> it = g.predecessors(2);
        assertEquals(3, it.next().intValue());
        assertEquals(1, it.next().intValue());
        it = g.predecessors(3);
        assertEquals(1, it.next().intValue());
        assertEquals(2, it.next().intValue());
        it = g.predecessors(4);
        assertEquals(2, it.next().intValue());
    }

    @Test
    public void testNeighbors() {
        UndirectedGraph u = new UndirectedGraph();
        u.add();
        u.add();
        u.add();
        u.add();
        u.add(1, 3);
        u.add(1, 2);
        u.add(2, 4);
        u.add(2, 3);
        Iteration<Integer> it = u.neighbors(1);
        assertEquals(3, it.next().intValue());
        assertEquals(2, it.next().intValue());
        it = u.successors(2);
        assertEquals(1, it.next().intValue());
        assertEquals(4, it.next().intValue());
        assertEquals(3, it.next().intValue());
    }

    @Test
    public void testEdges() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        Iteration<int[]> it = g.edges();
        int[] actual = it.next();
        assertEquals(1, actual[0]);
        assertEquals(2, actual[1]);
        actual = it.next();
        assertEquals(1, actual[0]);
        assertEquals(3, actual[1]);
        actual = it.next();
        assertEquals(2, actual[0]);
        assertEquals(3, actual[1]);

        UndirectedGraph u = new UndirectedGraph();
        u.add();
        u.add();
        u.add();
        u.add();
        u.add(1, 2);
        u.add(1, 2);
        u.add(2, 1);
        u.add(1, 3);
        u.add(3, 1);
        it = u.edges();
        actual = it.next();
        assertEquals(1, actual[0]);
        assertEquals(2, actual[1]);
        actual = it.next();
        assertEquals(1, actual[0]);
        assertEquals(3, actual[1]);
    }


    @Test
    public void testEdgeId() {
        DirectedGraph g = new DirectedGraph();
        assertEquals(0, g.edgeId(1, 2));
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(3, 2);
        assertEquals(1, g.edgeId(1, 2));
        assertEquals(2, g.edgeId(1, 3));
        assertEquals(3, g.edgeId(3, 2));

        UndirectedGraph u = new UndirectedGraph();
        assertEquals(0, u.edgeId(1, 2));
        u.add();
        u.add();
        u.add();
        u.add(1, 2);
        u.add(1, 3);
        u.add(2, 3);
        assertEquals(1, u.edgeId(1, 2));
        assertEquals(1, u.edgeId(2, 1));
        assertEquals(2, u.edgeId(1, 3));
        assertEquals(2, u.edgeId(3, 1));
        assertEquals(3, u.edgeId(3, 2));
        assertEquals(3, u.edgeId(2, 3));
    }

    @Test
    public void inOutDegree() {
        DirectedGraph g = new DirectedGraph();
        for (int i = 0; i < 5; i++) {
            g.add();
        }
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        g.add(1, 1);
        g.add(3, 4);
        g.add(4, 3);
        assertEquals(1, g.inDegree(1));
        assertEquals(0, g.inDegree(5));
        assertEquals(3, g.outDegree(1));
    }

    @Test
    public void testAddEdges() {
        DirectedGraph g = new DirectedGraph();
        g.add();
        g.add();
        g.add();
        g.add(1, 2);
        g.add(1, 3);
        g.add(2, 3);
        g.add(1, 1);
        assertEquals(3, g.vertexSize());
        assertEquals(4, g.edgeSize());
    }
}
