package graph;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/** Unit tests for the graph traversals
 *  @author J. S. Pimenta
 */
public class TraversalTesting {

    private class BreadthFirstClient extends BreadthFirstTraversal {

        BreadthFirstClient(Graph G) {
            super(G);
            _path = new LinkedList<>();
        }

        @Override
        protected boolean visit(int v) {
            _path.add(v);
            return true;
        }

        private LinkedList<Integer> _path;
    }

    private class DepthFirstClient extends DepthFirstTraversal {

        DepthFirstClient(Graph G) {
            super(G);
            _path = new LinkedList<>();
        }

        @Override
        protected boolean visit(int v) {
            _path.add(v);
            return true;
        }

        @Override
        protected boolean shouldPostVisit(int v) {
            return false;
        }

        private LinkedList<Integer> _path;
    }

    private class DepthFirstPostClient extends DepthFirstTraversal {

        DepthFirstPostClient(Graph G) {
            super(G);
            _path = new LinkedList<>();
        }

        @Override
        protected boolean postVisit(int v) {
            _path.add(v);
            return true;
        }

        @Override
        protected boolean shouldPostVisit(int v) {
            return true;
        }

        private LinkedList<Integer> _path;
    }

    @Test
    public void testClientTraversal1() {
        DirectedGraph dg = new DirectedGraph();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add(1, 2);
        dg.add(1, 3);
        dg.add(1, 4);
        dg.add(2, 5);
        dg.add(3, 5);
        dg.add(5, 6);
        dg.add(4, 6);
        dg.add(4, 7);

        DepthFirstClient dfsClient = new DepthFirstClient(dg);
        dfsClient.traverse(1);
        LinkedList<Integer> dfsVisited = dfsClient._path;
        LinkedList<Integer> dfsExpected = new LinkedList<>();
        dfsExpected.add(1);
        dfsExpected.add(4);
        dfsExpected.add(7);
        dfsExpected.add(6);
        dfsExpected.add(3);
        dfsExpected.add(5);
        dfsExpected.add(2);
        assertEquals(dfsVisited, dfsExpected);
    }

    @Test
    public void testClientTraversal2() {
        DirectedGraph dg = new DirectedGraph();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add(1, 2);
        dg.add(1, 3);
        dg.add(1, 4);
        dg.add(2, 5);
        dg.add(3, 5);
        dg.add(5, 6);
        dg.add(4, 6);
        dg.add(4, 7);

        BreadthFirstClient bfsClient = new BreadthFirstClient(dg);
        bfsClient.traverse(1);
        LinkedList<Integer> bfsVisited = bfsClient._path;
        LinkedList<Integer> bfsExpected = new LinkedList<>();
        bfsExpected.add(1);
        bfsExpected.add(2);
        bfsExpected.add(3);
        bfsExpected.add(4);
        bfsExpected.add(5);
        bfsExpected.add(6);
        bfsExpected.add(7);
        assertEquals(bfsVisited, bfsExpected);
    }

    @Test
    public void testClientTraversal3() {
        DirectedGraph dg = new DirectedGraph();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add();
        dg.add(1, 2);
        dg.add(1, 5);
        dg.add(1, 6);
        dg.add(2, 4);
        dg.add(4, 3);
        dg.add(4, 5);
        dg.add(6, 5);

        DepthFirstPostClient dfsPostClient = new DepthFirstPostClient(dg);
        dfsPostClient.traverse(1);
        LinkedList<Integer> dfsPostVisited = dfsPostClient._path;
        LinkedList<Integer> dfsPostExpected = new LinkedList<>();
        dfsPostExpected.add(5);
        dfsPostExpected.add(6);
        dfsPostExpected.add(3);
        dfsPostExpected.add(4);
        dfsPostExpected.add(2);
        dfsPostExpected.add(1);
        assertEquals(dfsPostVisited, dfsPostExpected);
    }
}
